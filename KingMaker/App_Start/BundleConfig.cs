﻿using System.Web;
using System.Web.Optimization;

namespace KingMaker
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/_Resources/jQuery/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/_Resources/bootstrap/js/bootstrap.js"));

            bundles.Add(new StyleBundle("~/_Resources/css").Include(
                      "~/_Resources/bootstrap/css/bootstrap.css",
                      "~/_Resources/bootstrap/css/bootstrap-theme.css",
                      "~/_Resources/font-awesome/css/font-awesome.css",
                      "~/_Resources/ion-icons/css/ionicons.css",
                      "~/_Resources/AdminLTE/css/AdminLTE.css",
                      "~/_Resources/AdminLTE/css/skins/skin-green-light.css"));
        }
    }
}
