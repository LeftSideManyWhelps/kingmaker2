﻿namespace KingMaker.Models
{
    public class Card
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Delay { get; set; }
    }
}