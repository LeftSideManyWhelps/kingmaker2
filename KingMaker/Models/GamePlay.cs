﻿using System;
using System.Collections.Generic;

namespace KingMaker.Models
{
    public class GamePlay
    {
        private List<Card> _cardPool { get; set; }
        private Card _attackCard = new Card
        {
            Title = "Bonk",
            Attack = 1,
            Defense = 0,
            Description = "Deal attack damage",
            Delay = 0,
            Image = "Bonk"
        };

        private Card _defenseCard = new Card
        {
            Title = "Block",
            Attack = 0,
            Defense = 1,
            Description = "Build Defense",
            Delay = 0,
            Image = "Block"
        };

        public Player Hero { get; set; }
        public Player Enemy { get; set; }

        public void NewGame()
        {
            BuildCardPool();

            Hero = new Player();
            Hero.Deck = new Deck();

            Enemy = new Player();
            Enemy.Deck = new Deck();

            Hero.Deck.CreateCardDeck(_cardPool);
            Hero.Name = "Hero";
            Hero.Health = 50;
            Hero.Defense = 1;
            Hero.Hand = new Hand();
            Hero.Hand.Cards = new Dictionary<int, Card>();

            for (var i = 0; i < 5; i++)
            {
                Hero.Hand.Cards.Add(i, _cardPool[GetRandomCardFromCardPool()]);
            }

            Enemy.Deck.CreateCardDeck(_cardPool);
            Enemy.Name = "Bad Guy";
            Enemy.Health = 6;
            Enemy.Defense = 0;
            Enemy.Hand = new Hand();
            Enemy.Hand.Cards = new Dictionary<int, Card>();

            for (var i = 0; i < 5; i++)
            {
                Enemy.Hand.Cards.Add(i, _cardPool[GetRandomCardFromCardPool()]);
            }

        }

        private int GetRandomCardFromCardPool()
        {
            return new Random().Next(0, _cardPool.Count);
        }

        private void BuildCardPool()
        {
            _cardPool = new List<Card>();

            for (var i = 0; i < 10; i++)
            {
                var attackCard = _attackCard;

                attackCard.Attack = new Random().Next(1, 3);
                _cardPool.Add(attackCard);
            }

            for (var i = 0; i < 10; i++)
            {
                var defenseCard = _defenseCard;

                defenseCard.Defense = new Random().Next(1, 3);
                _cardPool.Add(defenseCard);
            }
        }
    }
}