﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KingMaker.Models
{
    public class Deck
    {
        public Dictionary<int, Card> Cards { get; set; }

        public void CreateCardDeck(List<Card> cardPool)
        {
            Cards = new Dictionary<int, Card>();

            if (cardPool.Count > 0)
            {
                for (var i = 0; i < 20; i++)
                {
                    Cards.Add(i, cardPool[GetRandomCardFromDeck()]);
                }
            }
        }

        public void AddCardToDeck(Card addCard)
        {
            Cards.Add(Cards.Count + 1, addCard);
        }

        public Card DrawCard()
        {
            var card = new Card();
            var drawSlot = GetRandomCardFromDeck();

            card = Cards[drawSlot];
            Cards.Remove(drawSlot);

            return card;
        }

        private int GetRandomCardFromDeck()
        {
            return new Random().Next(0, Cards.Count);
        }
    }
}