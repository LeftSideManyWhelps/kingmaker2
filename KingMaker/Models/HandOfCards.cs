﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KingMaker.Models
{
    public class Hand
    {
        public Dictionary<int, Card> Cards { get; set; }
    }
}