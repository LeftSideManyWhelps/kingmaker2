﻿namespace KingMaker.Models
{
    public class Player
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int Defense { get; set; }
        
        public Deck Deck { get; set; }
        public Hand Hand { get; set; }
    }
}