﻿using KingMaker.Models;
using System.Web.Mvc;

namespace KingMaker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            GamePlay game = new GamePlay();

            game.NewGame();

            return View( game );
        }
    }
}